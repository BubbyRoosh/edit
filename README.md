# Edit!

A small and simple editor by [c9x](https://c9x.me/edit/) with some simple
quality-of-life improvements!

## Changes

* Simple config.h implementation (tag contents, colors, font, tab width)
* Applied [wintile patch](http://c9x.me/edit/patches/wintitle.diff).
* `Res` command (resize X)
* `Cd` command (chdir)
* Vi 'r' key functionality (replace)
* Vi '~' key functoinality (swap-case)
* Better handling of command-line arguments (directories/new files)

## Building

Requirements: `make` and some CWeb implementation.
Running `make` will result in a binary being placed in `./obj/edit`

More detailed-ish build instructions are provided on the website above.

## Screenshot

![](./ss.png)
