#ifndef CONFIG_H__
#define CONFIG_H__

/* Defaults */
static const unsigned char TagInit[] = "Look \n" \
									   "Get \n" \
									   "New\n" \
									   "|fmt\n";

static const char FONTNAME[] = "noto mono:pixelsize=14";
static const GColor TEXTCOLOR = GBlack;
static const GColor SELECTEDCOLOR = GPaleBlue;
static const GColor CURSORCOLOR = GXBlack;
static const GColor BGCOLOR = GPaleYellow;
static const GColor TAGCOLOR = GPaleGreen;

static const int TabWidth = 8;
static const int TagRatio = 3;
/* End Defaults */

/*
static const unsigned char TagInit[] = "Look \n" \
									   "Get \n" \
									   "New\n" \
									   "|fmt\n";

static const char FONTNAME[] = "Iosevka:pixelsize=14";
static const GColor TEXTCOLOR = GWhite;
static const GColor SELECTEDCOLOR = (GColor){40, 44, 67, 0};
static const GColor CURSORCOLOR = GXWhite;
static const GColor BGCOLOR = GBlack;
static const GColor TAGCOLOR = GBlack;

static const int TabWidth = 4;
static const int TagRatio = 3;
*/

#endif /* CONFIG_H__ */
